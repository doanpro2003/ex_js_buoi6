var a = [];
document.getElementById('add-number').onclick = function () {
    var num = document.getElementById("txt-num");
    // kiểm tra rỗng
    if (num.value == "")
        return;
    var number = num.value * 1;
    // reset value
    num.value = "";
    a.push(number);
    document.getElementById("result").innerHTML = `👉 ${a}`;
}
// Bài 1
document.getElementById('sum').onclick = function () {
    var s = 0;
    for (var i = 0; i < a.length; i++) {
        if (a[i] > 0)
            s += a[i];
    }
    document.getElementById("result1").innerHTML = `👉 ${s}`;
}
// Bài 2
document.getElementById('count-positive').onclick = function () {
    var count = 0;
    for (var i = 0; i < a.length; i++) {
        if (a[i] > 0)
            count++;
    }
    document.getElementById("result2").innerHTML = `👉 ${count}`;
}
// Bài 3
document.getElementById('min-num').onclick = function () {
    var minNum = a[0];
    for (var i = 0; i < a.length; i++) {
        if (a[i] < minNum)
            minNum = a[i];
    }
    document.getElementById("result3").innerHTML = `👉 ${minNum}`;
}
// Bài 4
function checkpositive(n) {
    if (n > 0)
        return 1;
    return 0;
}
function first_positive_num() {
    var first_positive;
    for (var i = 0; i < a.length; i++) {
        if (checkpositive(a[i]) == 1) {
            first_positive = i;
            return first_positive;
        }
    }
    return -1;
}
document.getElementById('min-num-positive').onclick = function () {
    if (first_positive_num() == -1) {
        document.getElementById("result4").innerHTML = `👉 Không có số dương trong mảng`;
    }
    else {
        var minPositive = a[first_positive_num()];
        for (var i = 0; i < a.length; i++) {
            if (a[i] < minPositive && a[i] > 0) {
                minPositive = a[i];
            }
        }
        document.getElementById("result4").innerHTML = `👉Số dương nhỏ nhất: ${minPositive}`;
    }

}
// Bài 5
function lastEvenNum() {
    for (var i = a.length - 1; i >= 0; i--) {
        if (a[i] % 2 == 0 && a[i] >= 0) {
            return a[i];
        }
    }
    return -1;
}
document.getElementById('last-even-num').onclick = function () {
    if (lastEvenNum() == -1) {
        document.getElementById("result5").innerHTML = "Không có số chẵn cuối";
    }
    else
        document.getElementById("result5").innerHTML = `👉Số chẵn cuối cùng: ${lastEvenNum()}`;
}

// Bài 6

document.getElementById('swap').onclick = function () {

    var location_1 = document.getElementById("txt-location1").value;

    var location_2 = document.getElementById("txt-location2").value;

    var temp = a[location_1];

    a[location_1] = a[location_2];

    a[location_2] = temp;

    document.getElementById("result6").innerHTML = `👉Mảng sau khi đổi: ${a}`;

}

// Bài 7
document.getElementById('Sort').onclick = function () {

    var temp;
    for (var i = 0; i < a.length; i++) {
        for (var j = i + 1; j < a.length; j++)
            if (a[i] > a[j]) {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
    }
    document.getElementById("result7").innerHTML = `👉Mảng sau khi sắp xếp: ${a}`;

}

// Bài 8

function demcacuocso(n) {
    var count = 0;
    for (var i = 1; i <= n; i++) {
        if (n % i == 0) {
            count++;
        }
    }
    return count;
}

function ktsnt(n) {
    if (demcacuocso(n) == 2)
        return 1;
    return 0;
}


document.getElementById('SoNguyenTo').onclick = function () {

    var content = "";
    for (var i = 0; i < a.length; i++) {
        if (ktsnt(a[i]) == 1) {
            content = `👉 Số nguyên tố đầu tiên: ${a[i]}`;
            break;
        }
    }
    if (ktsnt(a[i]) == 0)
        return (document.getElementById("result8").innerHTML = `👉 Mảng không có số nguyên tố`);
    document.getElementById("result8").innerHTML = content;
}

// Bài 9

var b = [];
document.getElementById('add-num').onclick = function () {
    var input = document.getElementById("txt-number");
    if (input.value == "")
        return;
    var number = input.value * 1;
    b.push(number);
    input.value = "";
    document.getElementById("result9").innerHTML = `👉 ${b}`;
}
document.getElementById('count-number').onclick = function () {
    var count = 0;
    for (var i = 0; i < b.length; i++) {
        if (Number.isInteger(b[i]))
            count++;
    }
    document.getElementById("result-count").innerHTML = `👉Số nguyên: ${count}`;
}
// Bài 10
document.getElementById('compare').onclick = function () {
    var content = "";
    var countPositive = 0;
    var countNegative = 0;
    for (var i = 0; i < a.length; i++) {
        if (a[i] > 0)
            countPositive++;
        if (a[i] < 0)
            countNegative++;
    }
    if (countNegative > countPositive) {
        content = `👉Số âm > Số dương`;
    } else if (countNegative < countPositive) {
        content = `👉Số âm < Số dương`;
    } else {
        content = `👉Số âm = Số dương`;
    }
    document.getElementById("result10").innerHTML = content;
}
